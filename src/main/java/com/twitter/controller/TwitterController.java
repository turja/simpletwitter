package com.twitter.controller;

import com.twitter.Application;
import com.twitter.dto.Tweet;
import com.twitter.service.TwitterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tweet")
public class TwitterController {
    @Autowired
    private TwitterService twitterService;

    private static final Logger log = LogManager.getLogger(TwitterController.class);

    @PostMapping("/create")
    public Tweet create(@RequestBody String tweetText) {
        log.debug("Received create tweet request");
        return twitterService.createTweet(tweetText);
    }

    @GetMapping("/get")
    public List<Tweet> getTweets() {
        log.debug("Received get tweet request");
        return twitterService.getTweetList(0);
    }

    @PostMapping("/{id}/retweet")
    public @ResponseBody Tweet reTweet(@PathVariable("id") String id) {
        log.debug("Received retweet request for id: " + id);
        return twitterService.reTweet(id);
    }

    @GetMapping("/get/top/{number}")
    public List<Tweet> getTopTweets(@PathVariable("number") int number) {
        log.debug("Received get top " + number + " tweet request");
        return twitterService.getTweetList(number);
    }
}
