package com.twitter;

public class ApplicationConstant {
    public static final int MAX_CACHE_LIST_LENGTH = 5000;
    public static final String TWEET_KEY = "tweet-cache";
}
