package com.twitter.service;

import com.twitter.controller.TwitterController;
import com.twitter.dto.Tweet;
import com.twitter.model.TweetEntity;
import com.twitter.repository.TweetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.twitter.ApplicationConstant.MAX_CACHE_LIST_LENGTH;
import static com.twitter.ApplicationConstant.TWEET_KEY;

@Service
public class TwitterService {
    @Autowired
    private TweetRepository tweetRepository;
    @Autowired
    RedisTemplate<String, TweetEntity> redisTemplate;

    private static final Logger log = LogManager.getLogger(TwitterService.class);

    public Tweet createTweet(String tweet) {
        TweetEntity tweetEntity = TweetEntity.builder()
                .content(tweet)
                .createdAt(LocalDateTime.now())
                .build();
        tweetEntity = tweetRepository.save(tweetEntity);
        log.info("Inserted new tweet into db, created id: " + tweetEntity.getId().toString());
        pushToCache(tweetEntity);

        return Tweet.builder()
                .id(tweetEntity.getId().toString())
                .content(tweetEntity.getContent())
                .createdAt(tweetEntity.getCreatedAt())
                .build();
    }

    public Tweet reTweet(String tweetId) {
        Optional<TweetEntity> optionalEntity = tweetRepository.findById(UUID.fromString(tweetId));
        if (optionalEntity.isEmpty()) {
            log.info("Twitter id Not Found for id: " + tweetId);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Twitter id Not Found for id " + tweetId);
        }
        TweetEntity entity = optionalEntity.get();
        TweetEntity newEntity = TweetEntity.builder()
                .createdAt(LocalDateTime.now())
                .content(entity.getContent())
                .originalTweetId(entity.getId())
                .build();
        newEntity = tweetRepository.save(newEntity);
        log.info("Inserted re-tweet entity into db, retweet id: " + newEntity.getId().toString());
        pushToCache(newEntity);
        return Tweet.builder()
                .id(newEntity.getId().toString())
                .content(newEntity.getContent())
                .originalTweetId(newEntity.getOriginalTweetId().toString())
                .createdAt(newEntity.getCreatedAt())
                .build();
    }

    public List<Tweet> getTweetList(int number) {
        List<TweetEntity> tweetList;
        if (number > redisTemplate.opsForList().size(TWEET_KEY)) {
            log.info("Cache doesn't have all records, therefore fetching from db...");
            Page<TweetEntity> page = tweetRepository.findAll(PageRequest.of(0, number, Sort.by(Sort.Order.desc("createdAt"))));
            tweetList = page.getContent();
            //update cache with fetched data from db
            refreshListToCache(tweetList);
        } else {
            //fetch from redis cache
            log.info("Fetching tweet list from cache...");
            tweetList = redisTemplate.opsForList().range(TWEET_KEY, 0, number - 1);
        }

        return tweetList.stream().map(entity -> Tweet.builder()
                .id(entity.getId().toString())
                .content(entity.getContent())
                .createdAt(entity.getCreatedAt())
                .originalTweetId(entity.getOriginalTweetId() != null ? entity.getOriginalTweetId().toString() : null)
                .build()).collect(Collectors.toList());
    }

    private void pushToCache(TweetEntity tweetEntity) {
        log.info("Pushing tweet into cache for id: " + tweetEntity.getId().toString());
        if (redisTemplate.opsForList().size(TWEET_KEY) > MAX_CACHE_LIST_LENGTH) {
            //remove some old tweets from cache, say 10%
            log.info("Cache size exceeds maximum limit, clearing old records from cache...");
            redisTemplate.opsForList().trim(TWEET_KEY, 0, MAX_CACHE_LIST_LENGTH / 10 * 9);
        }
        redisTemplate.opsForList().leftPush(TWEET_KEY, tweetEntity);
    }

    private void refreshListToCache(List<TweetEntity> tweetList) {
        log.info("Refreshing records from db into cache, record size: " + tweetList.size());
        redisTemplate.delete(TWEET_KEY);
        redisTemplate.opsForList().leftPushAll(TWEET_KEY, tweetList);
    }

}
