package com.twitter.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name= "tweets", indexes = {
        @Index(name = "i_created_at", columnList = "created_at")
})
public class TweetEntity implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", columnDefinition = "uuid")
    private UUID id;

    @Column(columnDefinition = "varchar(140)")
    private String content;

    @Column(name = "created_at", columnDefinition = "timestamp", updatable = false)
    private LocalDateTime createdAt;

    @Column(name="original_tweet_id", nullable = true, columnDefinition = "uuid")
    private UUID originalTweetId;
}
