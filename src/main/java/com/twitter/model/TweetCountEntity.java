package com.twitter.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@Builder
@Entity
public class TweetCountEntity {
    @Id
    private UUID tweetId;

    @Column(columnDefinition = "number")
    private Integer count;
}
