package com.twitter.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tweet {
    private String id;

    private String content;

    private LocalDateTime createdAt;

    private String originalTweetId;
}
