package com.twitter;

import com.twitter.dto.Tweet;
import com.twitter.utils.JsonUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TwitterControllerTest {

    private static final String PATH_PREFIX = "/tweet";
    private static final String CREATE_PATH = PATH_PREFIX + "/create";
    private static final String RETWEET_PATH = PATH_PREFIX + "/{id}/retweet";
    private static final String GET_TOP_N_PATH = PATH_PREFIX + "/get/top/{number}";

    @Autowired
    private MockMvc mvc;

    @Test
    public void createTweet() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(CREATE_PATH)
                .accept(MediaType.APPLICATION_JSON)
                .content("Sample test tweet"))
                .andExpect(status().isOk())
                .andReturn();
        Tweet tweet = JsonUtils.convertToObject(mvcResult.getResponse().getContentAsString(), Tweet.class);
        assertNotNull(tweet);
        assertNotNull(tweet.getId());
        assertEquals("Sample test tweet", tweet.getContent());
        assertNull(tweet.getOriginalTweetId());
    }

    @Test
    public void reTweet() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(CREATE_PATH)
                .accept(MediaType.APPLICATION_JSON)
                .content("Sample test tweet"))
                .andExpect(status().isOk())
                .andReturn();
        Tweet tweet = JsonUtils.convertToObject(mvcResult.getResponse().getContentAsString(), Tweet.class);
        assertNotNull(tweet);

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(RETWEET_PATH, tweet.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        Tweet reTweet = JsonUtils.convertToObject(mvcResult.getResponse().getContentAsString(), Tweet.class);
        assertNotNull(reTweet);
        assertNotNull(reTweet.getId());
        assertEquals("Sample test tweet", reTweet.getContent());
        assertEquals(tweet.getId(), reTweet.getOriginalTweetId());
    }

    @Test
    public void getTop5Tweets() throws Exception {
        generateTweets(5);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GET_TOP_N_PATH, 5)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        List<Tweet> tweetList = Arrays.asList(JsonUtils.convertToObject(mvcResult.getResponse().getContentAsString(), Tweet[].class));
        assertNotNull(tweetList);
        assertEquals(5, tweetList.size());
    }

    private void generateTweets(int count) throws Exception {
        for (int i = 0; i < count; i++) {
            mvc.perform(MockMvcRequestBuilders.post(CREATE_PATH)
                    .accept(MediaType.APPLICATION_JSON)
                    .content("Sample test tweet " + i))
                    .andExpect(status().isOk());
        }
    }
}
