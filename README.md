# Design Simple Twitter application
This project is to implement basic feature of a simple twitter.

## The Problem
```
The goal of this exercise is to Implement a very basic version of Twitter in an efficient manner.

```



# My solution

## Prerequisites
This project use maven as build tool.

JDK 1.8 is required to build and run the tool

## Development Tool
Intellij Idea was used for development. But it is not mandatory to run the application.
Following tech stacks are used for this implementation:
- H2 Database
- Redis Cache
- Postman

## Assumptions
This application assumes redis cache is installed and pre-configured.
Please use following instructions if redis is not installed in the machine.

docker pull redis/redis-stack

docker run -d --name redis-stack -p 6379:6379 -p 8001:8001 redis/redis-stack:latest


## How to build
download the project code from bitbucket

In command prompt/terminal go to 'SimpleTwitter' directory

Run following command

`mvn clean install`

Fat jar file SimpleTwitter-1.0-SNAPSHOT.jar will be created inside target/

###Run tests

`mvn clean test`

## Running application

`java -jar target/SimpleTwitter-1.0-SNAPSHOT.jar`

Otherwise, can open the project in Intellij Idea and run Application.java

### Logging
All logs are written to twitter.log. By default they are in debug mode. Apache log4j2 is used for logging


## Implementation
Limitations:
* API Gateway was not implemented for this application, can be implemented as future improvement
* Logs are not well defined.
* Error messages are not properly polished 


## Documents
- SimpleTwitterDesign.png: Contains design diagram
- Postman collection: Postman collection to test the api is in \test\java\postman directory




